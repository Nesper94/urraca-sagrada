# Hechos e historias sobre la Urraca Sagrada

Esta es una recopilación de hechos e historias sobre la Urraca Sagrada. Todo lo 
que aquí está contenido ha sido inspirado por el poderosísimo espíritu del Gran 
Bacalao Gigante.

Conoce a la Urraca y lleva su palabra en tu corazón.

# Hechos e historias sobre la Urraca Sagrada

## Hechos sobre la Urraca Sagrada
- La Urraca Sagrada tiene 4 alas.  
![urraca-sagrada-01](images/urraca-sagrada-01.jpg)
- US es la abreviación de Urraca Sagrada, por ejemplo "All about US" quiere 
decir "Todo sobre la Urraca Sagrada".
- Monesvol es en realidad un vómito de la Urraca Sagrada.

## Creación
Al principio solo existía un gran océano, y el Gran Bacalao Gigante (GBG) 
flotaba plácidamente sobre él. La Urraca, viendo el vacío infinito que la 
rodeaba, decidió crear el universo subiendo al lomo del Gran Bacalao Gigante.

![creación-01](images/la-creación.jpg)
![creación-02](images/urraca-gbg-618x800.png)

## Urraquistas famosos
La banda Queen es urraquista, su canción 'We will rock you' es en realidad un 
manifiesto urraquista que dice 'We will, we will (ur)raquiu (ur)raquiu', es 
decir 'vamos a urraquizarte' solo que para evitar persecusiones religiosas 
dejaron la canción como la conocemos.

## El fin
Al final de los tiempos vendrá Bender con el Gran Búho Cósmico a realizar los 
últimos designios de la Urraca en el mundo, y podremos ver de nuevo la cola del 
Gran Bacalao Gigante.

![profecía-del-fin](images/profecía-bender-búho-cósmico.jpg)
